#include <gtkmm.h>
#include <iostream>

/* This is only for installing ui with cmake */
/* For testing propose use the local (not installed) ui file */
//#include "config.h"

Gtk::Window*  pWindow   = 0;
Gtk::Button*  pButton1  = 0;
Gtk::Button*  pButton2  = 0;
Gtk::Button*  pButton3  = 0;
Gtk::Button*  pButton4  = 0;
Gtk::Spinner* pSpinner1 = 0;

static void on_button1_clicked();
static void on_button2_clicked();
static void on_button3_clicked();
static void on_button4_clicked();


int main (int argc, char **argv)
{
  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "org.gtkmm.example");
  Glib::RefPtr<Gtk::Builder> refBuilder = Gtk::Builder::create_from_file("spinner.ui");
  refBuilder->get_widget("window",  pWindow);
  if(pWindow)
  {
    refBuilder->get_widget("button1",  pButton1);
    refBuilder->get_widget("button2",  pButton2);
    refBuilder->get_widget("button3",  pButton3);
    refBuilder->get_widget("button4",  pButton4);
    refBuilder->get_widget("spinner1", pSpinner1);
    
    if(pButton1) pButton1->signal_clicked().connect(sigc::ptr_fun(on_button1_clicked));
    if(pButton2) pButton2->signal_clicked().connect(sigc::ptr_fun(on_button2_clicked));
    if(pButton3) pButton3->signal_clicked().connect(sigc::ptr_fun(on_button3_clicked));
    if(pButton4) pButton4->signal_clicked().connect(sigc::ptr_fun(on_button4_clicked));
   
   
   }

  app->run(*pWindow);
  delete pWindow;
  return 0;
}

static void on_button1_clicked ()
{  
   pSpinner1->show();
}

static void on_button2_clicked ()
{
   pSpinner1->hide();
}

static void on_button3_clicked ()
{
   pSpinner1->start();
}

static void on_button4_clicked ()
{
   pSpinner1->stop();
}
