Before attempting to install libgtkmm-3.0-dev, you might first need to install the following development packages:

libsigc++-2.0-dev
libgtk-3-dev
libglib2.0-dev
libatkmm-1.6-dev
libpangomm-1.4-dev
libcairomm-1.0-dev
libgdk-pixbuf2.0-dev

These dependecies have their own dependencies, including runtime libraries.
You also need to install:

build-essential
dpkg-config
cmake (optional)

How to build the sources using the g++ compiler:

g++ -Wall -g -export-dynamic -o spinner spinner.cc `pkg-config --libs --cflags gtkmm-3.0`

How to build the project using cmake:

mkdir build
cd build
cmake ../
make


