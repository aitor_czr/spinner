cmake_minimum_required(VERSION 3.0.2-1)

project(spinner)

cmake_policy(SET CMP0002 OLD)

set(CMAKE_C_COMPILER_INIT g++)
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/ModulesCMake")
set(proyectEx spinner.cpp)
set(CMAKE_INSTALL_PREFIX /usr)

#set_source_files_properties(src/*.cpp PROPERTIES LANGUAGE CXX )

#if( CMAKE_SIZEOF_VOID_P EQUAL 8)
#   set(_ARCH "x86_64")
#else( CMAKE_SIZEOF_VOID_P EQUAL 8 ) 
#   set(_ARCH "i386")
#endif( CMAKE_SIZEOF_VOID_P EQUAL 8 )

include_directories(${CMAKE_SOURCE_DIR} ${GTKMM_INCLUDE_DIRS}
                    ${X11_INCLUDE_DIR} ${CAIRO_INCLUDE_DIRS}
	            ${GLIB_INCLUDE_DIRS} ${PANGO_INCLUDE_DIRS})

find_package(PkgConfig REQUIRED)
pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
#add_subdirectory (src)

link_directories(${GTKMM_LIBRARY_DIRS})
include_directories(${GTKMM_INCLUDE_DIRS})
add_definitions(${GTKMM_CFLAGS_OTHER})

##CONFIGURE_FILE("${PROJECT_SOURCE_DIR}/config.h.cmake" "${PROJECT_SOURCE_DIR}/config.h")

set(CMAKE_CXX_FLAGS "-Wall -g -export-dynamic -o spinner.cpp")
set(CMAKE_CXX_FLAGS_DEBUG "-Wall -g -export-dynamic -o spinner.cpp")

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/../)
set(CMAKE_BUILD_TYPE debug)
add_executable(${PROJECT_NAME} ${proyectEx})
target_link_libraries(${PROJECT_NAME} ${GTKMM_LIBRARIES})

##install (TARGETS spinner DESTINATION bin)
##install(FILES src/spinner.ui DESTINATION ${UI_FILE}share/spinner/ui)

##CONFIGURE_FILE( "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in" "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake" IMMEDIATE @ONLY)
##ADD_CUSTOM_TARGET(uninstall "${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake" )
